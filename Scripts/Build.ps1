[CmdletBinding()]
param(
  [Parameter()]
  [ValidateNotNullOrEmpty()]
  [string] $ModulesOutPath = "../dist/"
)

begin {
  function Merge-ContentInto {
    param(
      [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
      [ValidateNotNullOrEmpty()]
      [string] $File,

      [Parameter(Mandatory = $true)]
      [ValidateNotNullOrEmpty()]
      [string] $OutFile
    )

    process {
      if (Test-Path -Path $_) {
        Get-Content $_ | Add-Content $OutFile;
        Add-Content $OutFile -Value "";
      }
    }
  }
}

process {
  $ErrorActionPreference = 'Stop';
  $InformationPreference = 'Continue';

  $Modules = @(
    @{
      Name = 'Ancora.Tools.Docs';
    },
    @{
      Name = 'Ancora.Tools.Misc';
      ClassFiles = @(
        "OAuthToken"
      );
      RequiredModules = @('Az.KeyVault')
    }
  );

  $Modules | ForEach-Object {
    $Module = $_;

    $ModuleSourcePath = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, "../modules", $Module.Name));
    $ModuleOutPath = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $ModulesOutPath, $Module.Name));
    $ModuleOutFile = [System.IO.Path]::Combine($ModuleOutPath, "$($Module.Name).psm1");

    <# Clean Publish folder #>
    if (Test-Path $ModuleOutPath) {
      Write-Information "Delete old files from $ModuleOutPath";
      Get-ChildItem "$ModuleOutPath/*" -Recurse | Remove-Item -Force -Recurse | Out-Null
    }
    else {
      Write-Information "Creating $ModuleOutPath";
      New-Item $ModuleOutPath -ItemType Directory
    }

    <# Publish the module #>
    $ContentFiles = @(
      "$ModuleSourcePath/$($Module.Name).psd1",
      "$ModuleSourcePath/$($Module.Name).png",
      "$ModuleSourcePath/../LICENSE"
    )

    # Class files are sequence sensitive
    $ClassPath = [System.IO.Path]::Combine($ModuleSourcePath, "./internal/classes");

    $ScriptPaths = @(
      "internal/functions",
      "functions"
    )

    $ScriptFiles = $ScriptPaths | ForEach-Object { Get-ChildItem -Path ([System.IO.Path]::Combine($ModuleSourcePath, $_)) -Recurse -Include "*.ps1" -ErrorAction Ignore }

    Write-Information "Merge class files into $ModuleOutFile";
    $Module.ClassFiles | ForEach-Object { [System.IO.Path]::Combine($ClassPath, "$_.ps1") } | Merge-ContentInto -OutFile $ModuleOutFile;

    Write-Information "Merge script files into $ModuleOutFile";
    $ScriptFiles | ForEach-Object { $_.FullName } | Merge-ContentInto -OutFile $ModuleOutFile;

    Write-Information "Copy content files";
    $ContentFiles | ForEach-Object {
      if (Test-Path -Path $_) {
        Copy-Item -Path $_ -Destination "$ModuleOutPath/"
      }
    }

    # if ($SignModule) {
    #   Write-Information "Signing all *.ps* files"
    #   $files = "$ModuleOutPath/*.ps*"
    #   $cert = Get-Item Cert:\CurrentUser\My\39BCA611578AD62BA5126A406DBD4CC5DAFB859C

    #   Set-AuthenticodeSignature -FilePath $files -Certificate $cert | Out-Null
    # }

    Write-Information "Finished building - running tests";

    <# Run tests #>
    & Invoke-Command -ScriptBlock {
      if ($Module.RequiredModules) {
        $Module.RequiredModules | ForEach-Object { Install-Module -Name $_ -ErrorAction 'Stop' -Force -AcceptLicense -AllowClobber; }
      }

      Import-Module "$ModuleOutPath\$($Module.Name).psd1" -ErrorAction 'Stop';
      if ($null -ne (Get-Module PSScriptAnalyzer -ListAvailable)) {
        & Invoke-ScriptAnalyzer -Path $ModuleOutFile;
      }
      # & Invoke-Pester -Path "$ModuleSourcePath\tests"
    }
  }
}



