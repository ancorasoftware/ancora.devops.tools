[CmdletBinding()]
param(
  [Parameter()]
  [ValidateNotNullOrEmpty()]
  [string] $ModulesInPath = "../dist/",


  [Parameter()]
  [ValidateNotNullOrEmpty()]
  [string] $PackagesOutPath = "../dist/"
)

process {
  $ErrorActionPreference = 'Stop';
  $InformationPreference = 'Continue';

  $ModulesInPath = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $ModulesInPath));
  $PackagesOutPath = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $PackagesOutPath));

  $ModuleManifestFiles = Get-ChildItem -Path $ModulesInPath -Recurse -Include "*.psd1";

  $TempRepoName = "Temp_Repo_" + [Guid]::NewGuid().ToString();
  Register-PSRepository -Name $TempRepoName -SourceLocation $PackagesOutPath -PublishLocation $PackagesOutPath -InstallationPolicy Trusted;
  try {
    $ModuleManifestFiles | ForEach-Object {
      Publish-Module -Path $_.Directory -Repository $TempRepoName -NuGetApiKey 'ABC123' -Force -ErrorAction SilentlyContinue;
    }
  }
  finally {
    Unregister-PSRepository -Name $TempRepoName
  }
}
