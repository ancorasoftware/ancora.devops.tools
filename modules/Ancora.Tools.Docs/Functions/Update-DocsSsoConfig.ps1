
function Update-DocsSsoConfig {
  <#
      .SYNOPSIS
      Configures Single Sign-On (SAML) for an ancoraDocs (web client) instance.

      .DESCRIPTION
      Configures Single Sign-On (SAML) for an ancoraDocs (web client) instance.
      This script functions by updating the application's web.config file and generating a SAML signing key file for the service.
      Use of this script requires admin access to the host machine.

      .PARAMETER SiteUrl
      The URL to the target ancoraDocs web client instance.

      .PARAMETER TenantId
      The Tenant Identifier GUID of the target Azure environment.

      .PARAMETER AppId
      The Application Identifier GUID of the Azure AD Enterprise Appplication associated with the target ancoraDocs web client instance.

      .PARAMETER AppId
      The Application Identifier GUID of the Azure AD Enterprise Appplication associated with the target ancoraDocs web client instance.

      .PARAMETER Company
      The name of the Company that owns taget ancoraDocs web client instance.
      This value is used when generating the SAML Signing key for the service.
      Defaults to "Ancora Software, Inc.".

      .PARAMETER Country
      The country code used when generating the SAML Signing key for the service.
      Defaults to "US".

      .PARAMETER State
      The state used when generating the SAML Signing key for the service.
      Defaults to "California".

      .PARAMETER City
      The city used when generatingthe SAML Signing key for the service.
      Defaults to "San Diego".

      .PARAMETER Department
      The company department or organziation used when generating the SAML signing key for the service.
      Defaults to "IT".

      .PARAMETER Days
      The number of days for which the generated SAML signing key is valid.
      Defaults to 365.

      .PARAMETER KeyFileBasename
      The base name of the file to write the generated SAML signing key to.
      Defaults to "Service".

      .PARAMETER ConfigFile
      The name of the configuration file to update.
      Defaults to "web.config".

      .PARAMETER ForceServiceCertificateGeneration
      Forces generation of a new SAML signing key, overwriting any existing key file.
  #>

  [CmdletBinding()]
  [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingWriteHost', '', Justification='Prompt use for input.')]
  param(
    [Parameter(Mandatory=$true)]
    [System.Uri]$SiteUrl,

    [Parameter(Mandatory=$true)]
    [System.Guid]$TenantId,

    [Parameter(Mandatory=$true)]
    [System.Guid]$AppId,

    [Parameter(Mandatory=$false)]
    [string]$Company = "Ancora Software, Inc.",

    [Parameter(Mandatory=$false)]
    [string]$Country = 'US',

    [Parameter(Mandatory=$false)]
    [string]$State = 'California',

    [Parameter(Mandatory=$false)]
    [string]$City = 'San Diego',

    [Parameter(Mandatory=$false)]
    [string]$Department = 'IT',

    [Parameter(Mandatory=$false)]
    [string]$Days = 365,

    [Parameter(Mandatory=$false)]
    [string]$KeyFileBasename = 'Service',

    [Parameter(Mandatory=$false)]
    [string]$ConfigFile = "web.config",

    [switch]$ForceServiceCertificateGeneration
  )

  $ErrorActionPreference = "Stop";

  $AppDataDir = "App_Data";

  $KeyFileBasename = [System.IO.Path]::GetFileNameWithoutExtension($KeyFileBasename);
  $CertFile = "$($AppDataDir)\$($KeyFileBasename).pfx";

  if (-not (Test-Path -Path $CertFile) -or $ForceServiceCertificateGeneration) {
    New-Item -ItemType Directory -Name $AppDataDir -Force | Out-Null;
    Push-Location -Path $AppDataDir;

    try
    {
      openssl req -nodes -x509 -newkey rsa:2048 -days $Days -keyout "$($KeyFileBasename).key" -out "$($KeyFileBasename).crt" -subj "/C=$($Country)/ST=$($State)/L=$($City)/O=$($Company)/OU=$($Department)/CN=$($Domain)";
      try {
        openssl pkcs12 -export -nodes -out "$($KeyFileBasename).pfx" -inkey "$($KeyFileBasename).key" -in "$($KeyFileBasename).crt" -passout pass:
      } finally {
        Remove-Item -Path "$($KeyFileBasename).key" -Force;
        Remove-Item -Path "$($KeyFileBasename).crt" -Force;
      }
    }
    finally
    {
      Pop-Location;
    }
  }

  # Normalize the Site URL by stripping any path.
  # Is this a good idea?? -SDG
  $UrlBuilder = [System.UriBuilder]::new($SiteUrl);
  $UrlBuilder.Path = "";
  $SiteUrl = $UrlBuilder.Uri;

  # Load web.config and get reference to root 'configuration' node.
  [xml]$Xml = Get-Content -Path $ConfigFile;
  $ConfigurationNode = $Xml.SelectSingleNode("//configuration");

  ## Update <appSettings>
  ################################################

  $AppSettingsNode = $ConfigurationNode.SelectSingleNode("appSettings");

  $UrlBuilder.Path = "Saml2/SignIn";
  $node = $AppSettingsNode.SelectSingleNode("add[@key='SsoLoginUrl']");
  $node.SetAttribute('value', $UrlBuilder.Uri.ToString());

  $node = $AppSettingsNode.SelectSingleNode("add[@key='SsoLoginEnabled']");
  $node.SetAttribute('value', 'true');

  ## Update <modules> under <system.webServer>
  ################################################

  $ModulesNode = $ConfigurationNode.SelectSingleNode("system.webServer/modules");

  # Find or add <add> element for systemsys module
  $node = $ModulesNode.SelectSingleNode("add[@name='Saml2AuthenticationModule']");
  if ($null -eq $node) {
    $node = $Xml.CreateElement('add');
    $ModulesNode.AppendChild($node) | Out-Null;
    $node.SetAttribute('name', 'Saml2AuthenticationModule');
  }

  $node.SetAttribute('type', 'Sustainsys.Saml2.HttpModule.Saml2AuthenticationModule, Sustainsys.Saml2.HttpModule');

  ## Update <sustainsys.saml2> node

  $SustainsysSaml2Node = $ConfigurationNode.SelectSingleNode("sustainsys.saml2");
  if ($null -eq $SustainsysSaml2Node) {
    $SustainsysSaml2Node = $Xml.CreateElement('sustainsys.saml2');
    $ConfigurationNode.AppendChild($SustainsysSaml2Node) | Out-Null;
  }

  # Set entityId an returnUrl attributes.
  $SustainsysSaml2Node.SetAttribute('entityId', $SiteUrl.ToString());

  $UrlBuilder.Path = "Account/SsoLogin";
  $SustainsysSaml2Node.SetAttribute('returnUrl', $UrlBuilder.Uri.ToString());

  # Update 'identityProviders' XML subtree
  #######################

  $IdentityProvidersNode = $SustainsysSaml2Node.SelectSingleNode("identityProviders");
  if ($null -eq $IdentityProvidersNode) {
    $IdentityProvidersNode = $Xml.CreateElement('identityProviders');
    $SustainsysSaml2Node.AppendChild($IdentityProvidersNode) | Out-Null;
  }


  # Find exisiting identity provider node or create new one.
  $IdpEntityId = "https://sts.windows.net/$($TenantId.ToString())/";
  $AzureIdpNode = $IdentityProvidersNode.SelectSingleNode("add[@entityId='$($IdpEntityId)']");
  if ($null -eq $AzureIdpNode) {
    $AzureIdpNode = $IdentityProvidersNode.SelectSingleNode("add[@entityId='http://ancoradocs.com/adfs/services/trust']");
  }
  if ($null -eq $AzureIdpNode) {
    $AzureIdpNode = $Xml.CreateElement('add');
    $IdentityProvidersNode.AppendChild($AzureIdpNode) | Out-Null;
  }

  $AzureIdpNode.SetAttribute('entityId', "https://sts.windows.net/$($TenantId.ToString())/");
  $SignInOutUrl = "https://login.microsoftonline.com/$($TenantId.ToString())/saml2"
  $AzureIdpNode.SetAttribute('signOnUrl', $SignInOutUrl);
  $AzureIdpNode.SetAttribute('logoutUrl', $SignInOutUrl);
  $AzureIdpNode.SetAttribute('allowUnsolicitedAuthnResponse', 'true');
  $AzureIdpNode.SetAttribute('binding', 'HttpRedirect');

  $SigningCertificateNode = $AzureIdpNode.SelectSingleNode("signingCertificate");
  if ($null -eq $SigningCertificateNode) {
    $SigningCertificateNode = $Xml.CreateElement('signingCertificate');
    $AzureIdpNode.AppendChild($SigningCertificateNode) | Out-Null;
  }

  $SigningCertificateNode.SetAttribute('fileName', '~/App_Data/AzureIdp.cer');

  $AzureIdpCommentNode = $Xml.CreateComment($AzureIdpNode.OuterXml);
  $IdentityProvidersNode.ReplaceChild($AzureIdpCommentNode, $AzureIdpNode) | Out-Null;


  # Update 'federations'  XML subtree
  #######################

  $FederationsNode = $SustainsysSaml2Node.SelectSingleNode("federations");
  if ($null -eq $IdentityProvidersNode) {
    $FederationsNode = $Xml.CreateElement('federations');
    $SustainsysSaml2Node.AppendChild($FederationsNode) | Out-Null;
  }

  $node = $FederationsNode.SelectSingleNode("add");
  if ($null -eq $node) {
    $node = $Xml.CreateElement('add');
    $FederationsNode.AppendChild($node) | Out-Null;
  }

  $IdpMetadataUrl = "https://login.microsoftonline.com/$($TenantId.ToString())/federationmetadata/2007-06/federationmetadata.xml?appid=$($AppId.ToString())";
  $node.SetAttribute('metadataLocation', $IdpMetadataUrl);

  # Update 'serviceCertificates'  XML subtree
  #######################

  $ServiceCertificatesNode = $SustainsysSaml2Node.SelectSingleNode("serviceCertificates");
  if ($null -eq $ServiceCertificatesNode) {
    $ServiceCertificatesNode = $Xml.CreateElement('serviceCertificates');
    $SustainsysSaml2Node.AppendChild($ServiceCertificatesNode) | Out-Null;
  }

  $node = $ServiceCertificatesNode.SelectSingleNode("add");
  if ($null -eq $node) {
    $node = $Xml.CreateElement('add');
    $ServiceCertificatesNode.AppendChild($node) | Out-Null;
  }

  $IdpMetadataUrl = "https://login.microsoftonline.com/$($TenantId.ToString())/federationmetadata/2007-06/federationmetadata.xml?appid=$($AppId.ToString())";
  $node.SetAttribute('fileName', '~/App_Data/Service.pfx');

  ## Save the XML back and we're done! :)
  ################################################

  # Backup our original configuration file
  $ConfigFileItem = Get-Item -Path $ConfigFile;

  $ConfigFile = $ConfigFileItem.FullName;
  $Timestamp = Get-Date -Format "yyyyMMdd_HHmmss";
  $BackupConfigFile = "$($ConfigFileItem.Basename).backup_$($Timestamp).$($ConfigFileItem.Extension)";
  Move-Item -Path $ConfigFileItem.FullName -Destination $BackupConfigFile;
  Write-Information "Wrote backup of original web.config to '$($BackupConfigFile)'.";

  # Write updated config which excludes Azure as an IdP. Wait for the user to download and save the IdP cert.
  $Xml.Save($ConfigFile);
  Write-Host 'Initial configuration update complete. You must now perform the following two steps:';
  $UrlBuilder.Path = "Saml2";
  Write-Host "  1. Download and save the Serivce Provider SAML metadata from $($UrlBuilder.Uri.ToString()) to a file.";
  Write-Host "  2. Upload the saved metadata file to the Azure Enterprise Application.";
  Write-Host "  3. Download and save the Azure-provided (base64) signing certificate for the Enterprise application to '~/App_Data/AzureIdp.cer'.";
  Write-Host "Please press a key once the above steps are completed to finalize configuration.";

  $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

  # Write updated config which includes Azure as an IdP.
  $IdentityProvidersNode.ReplaceChild($AzureIdpNode, $AzureIdpCommentNode) | Out-Null;

  $Xml.Save($ConfigFile);
  Write-Information 'Final configuration update complete.';
}
