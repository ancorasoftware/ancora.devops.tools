@{
	RootModule = 'Ancora.Tools.Docs.psm1'
	ModuleVersion = '0.2.0'
	GUID = '5C68788E-A24D-483B-8E64-10C1BE9F09B9'

	CompatiblePSEditions = @("Core", "Desktop")
	PowershellVersion = "5.1"

	Author = 'https://bitbucket.org/ancorasoftware/ancora.devops.tools'
	CompanyName = 'Ancora Software, Inc.'
	Copyright = '(c) 2022 Ancora Software, Inc. All rights reserved.'

	Description = "PowerShell tools supporing Ancora Software's ancoraDocs."

	# RequiredAssemblies = @(
	# 	'System.Net.Requests',
  #   'System.Security.Cryptography.X509Certificates'
	# )

  # RequiredModules = @(
  # )

	DefaultCommandPrefix = 'Anc'
	FunctionsToExport = @(
    'Update-DocsSsoConfig'
	)

	# Private data to pass to the module specified in RootModule/ModuleToProcess. This may also contain a PSData hashtable with additional module metadata used by PowerShell.
	PrivateData = @{
		PSData = @{
			# Tags applied to this module. These help with module discovery in online galleries.
			Tags = @('Ancora', 'ancoraDocs', 'powershell', 'support')

			# License for this module.
			LicenseUri = 'https://bitbucket.org/ancorasoftware/ancora.devops.tools/raw/master/LICENSE'

			# A URL to the main website for this project.
			ProjectUri = 'https://bitbucket.org/ancorasoftware/ancora.devops.tools'

			# An icon representing this module.
			IconUri = 'https://bitbucket.org/ancorasoftware/ancora.devops.tools/raw/master/module/ancora.devops.tools.png'

      # External module depdendencies
      # ExternalModuleDependencies = @('Az.KeyVault')

		} # End of PSData hashtable

	} # End of PrivateData hashtable
}
