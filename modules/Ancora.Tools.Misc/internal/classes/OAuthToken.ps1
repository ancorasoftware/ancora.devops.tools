class OAuthToken {
  OAuthToken() {}

  OAuthToken($AccessToken, $RefreshToken, $Scopes)
  {
    $this.AccessToken = $AccessToken;
    $this.RefreshToken = $RefreshToken;
    $this.Scopes = $Scopes;
  }

  [string] $AccessToken;

  [string] $RefreshToken;

  [string] $Scopes;
}
