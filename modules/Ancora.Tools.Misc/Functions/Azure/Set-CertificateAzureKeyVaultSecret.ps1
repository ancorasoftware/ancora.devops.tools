function Set-CertificateAzureKeyVaultSecret {
  <#
    .SYNOPSIS
      Create a secret a in an Azure key vault representing an unsecured PKCS12 certificate from a secured local certificate file.

    .PARAMETER VaultName
      Specifies the name of the key vault to which this secret belongs. This cmdlet
      constructs the FQDN of a key vault based on the name that this parameter specifies
      and your current environment.

    .PARAMETER Name
      Specifies the name of a secret to modify. This cmdlet constructs the fully qualified
      domain name (FQDN) of a secret based on the name that this parameter specifies,
      the name of the key vault, and your current environment.

    .PARAMETER Path
      Specifies the path of the certificate to set as a secret.

    .PARAMETER Password
      Certificate password.

    .EXAMPLE
      PS> Set-AncCertificateAzureKeyVaultSecret -Path 'my-cert.pfx' -VaultName 'my-vault' -Name 'my-secret' -Password (ConvertTo-SecureString -String 'my-cert-password' -AsPlainText -Force);

    .NOTES
      This script is based on https://stackoverflow.com/a/34186811 and complements ConvertFrom-AncBase64EncodedCertificate
  #>
  [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseShouldProcessForStateChangingFunctions', '', Justification = "Creates in-memory object only.")]
  [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingConvertToSecureStringWithPlainText', '', Justification = "Required to convert certificate content to secret.")]
  [CmdletBinding(SupportsShouldProcess)]
  param(
    [Parameter(Mandatory=$true)]
    [string]$VaultName,

    [Parameter(Mandatory=$true)]
    [string]$Name,

    [Parameter(Mandatory=$true)]
    [string]$Path,

    [Parameter(Mandatory=$true)]
    [securestring]$Password
  )

  $ErrorActionPreference = 'Stop';

  $PasswordBstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Password);
  $PasswordInsecure = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($PasswordBstr);

  $Flags = [System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::Exportable;
  $CertCollection = [System.Security.Cryptography.X509Certificates.X509Certificate2Collection]::new();
  $CertCollection.Import($Path, $PasswordInsecure, $Flags);

  $CertContentType = [System.Security.Cryptography.X509Certificates.X509ContentType]::Pkcs12;
  $CertContent = $CertCollection.Export($CertContentType);

  $CertContentEncoded = [System.Convert]::ToBase64String($CertContent);
  $CertSecret = ConvertTo-SecureString -String $CertContentEncoded -AsPlainText -Force;
  $CertSecretType = 'application/x-pkcs12';
  Set-AzKeyVaultSecret -VaultName $VaultName -Name $Name -SecretValue $CertSecret -ContentType $CertSecretType -WhatIf:$WhatIfPreference;
}

