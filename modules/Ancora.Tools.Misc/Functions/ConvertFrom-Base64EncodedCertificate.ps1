function ConvertFrom-Base64EncodedCertificate {
  <#
    .SYNOPSIS
      Converts a base-64 encoded representation of an unsecured certificate file and writes it to a local file as a secured certificate.

    .PARAMETER EncodedCertificate
      Base-64 encoded certificate content.

    .PARAMETER Path
      Specifies the path of the certificate to write.

    .PARAMETER Password
      Certificate password.

    .EXAMPLE

      PS> $MyBase64EncodedCert | ConvertFrom-AncBase64EncodedCertificate -Path 'my-cert.pfx' -Password (ConvertTo-SecureString -String 'my-cert-password' -AsPlainText -Force);

    .NOTES
      This script is based on https://docs.microsoft.com/en-us/azure/devops/pipelines/tasks/deploy/azure-key-vault?view=azure-devops#arguments
  #>
  param(
    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [string]$EncodedCertificate,

    [Parameter(Mandatory=$true)]
    [string]$Path,

    [Parameter(Mandatory=$true)]
    [securestring]$Password
  )

  process {
    $ErrorActionPreference = 'Stop';

    $CertContent = [System.Convert]::FromBase64String($EncodedCertificate);
    $CertCollection = [System.Security.Cryptography.X509Certificates.X509Certificate2Collection]::new();
    $CertCollection.Import($CertContent, $null, [System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::Exportable);

    $PasswordBstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Password);
    $PasswordInsecure = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($PasswordBstr);

    $ProtectedCertContent = $certCollection.Export([System.Security.Cryptography.X509Certificates.X509ContentType]::Pkcs12, $PasswordInsecure)
    [System.IO.File]::WriteAllBytes($Path, $ProtectedCertContent);
  }
}
