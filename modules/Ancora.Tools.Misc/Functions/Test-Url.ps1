function Test-Url {
  <#
    .SYNOPSIS
      Tests if URL response with a success status code.

    .PARAMETER Url
      The URL to test.

    .PARAMETER Timeout
      Timeout period. Defaults to -1 millisecond, meaning no timeout period is specified.

    .PARAMETER RetryDelay
      Delay period between retry attempts. Defaults to 1 second.

    .PARAMETER MaxRetries
      Maximum number of retries. Defaults to 0 meaning no retries.

    .PARAMETER ProgressFormat
      Format to use when reporting progress. Can be either 'Auto', 'Progress', or 'VSTS'.

    .EXAMPLE
      PS> Test-AncUrl -Url "https://google.com" -Timeout ([TimeSpan]::FromSeconds(10))
  #>
  [CmdletBinding()]
  [OutputType([bool])]
  [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingEmptyCatchBlock', '', Justification='Error is translated to $False return value')]
  [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingWriteHost', '', Justification='Write-Host required for VSTS')]
  param(
    [Parameter(Position=0, Mandatory=$true)]
    [Uri[]]$Urls,

    [Parameter(Mandatory=$false)]
    [TimeSpan]$Timeout = [TimeSpan]::FromMilliseconds(-1),

    [Parameter(Mandatory=$false)]
    [TimeSpan]$RetryDelay = [TimeSpan]::FromSeconds(1),

    [Parameter(Mandatory=$false)]
    [int]$MaxRetries = 0,

    [ValidateSet('Auto','VSTS', 'Progress')]
    [string]$ProgressFormat = 'Auto'
  )

  $ErrorActionPreference = 'Stop';

  # Determine which progress output format to use.
  if ([string]::Equals($ProgressFormat, 'Auto', [System.StringComparison]::OrdinalIgnoreCase))
  {
    if (![string]::IsNullOrEmpty([System.Environment]::GetEnvironmentVariable('SYSTEM_TEAMPROJECTID')))
    {
      $ProgressFormat = 'VSTS';
    } else {
      $ProgressFormat = 'Progress';
    }
  }

  $TimeoutSet  = $Timeout.TotalMilliseconds -gt 0;
  $MaxRetriesSet = $MaxRetries -gt 0;
  $RetriesEnabled = $TimeoutSet -or $MaxRetriesSet;

  $Stopwatch = [System.Diagnostics.Stopwatch]::StartNew();

  $IsSuccessStatus = $false;

  $Retry = 0;
  do {

    # TODO: Improve code to not sleep after last loop iteration.
    if ($Retry -gt 0) {
      Start-Sleep -Milliseconds $RetryDelay.TotalMilliseconds;
    }

    # Test all URLs. All URLs must respond with success.
    ForEach ($Url in $Urls) {
      try {
        $Request = [System.Net.WebRequest]::Create($Url);
        $Response = $Request.GetResponse();
        try {
          $IsSuccessStatus = ([int]$Response.StatusCode / 100) -eq 2;
          if (!$IsSuccessStatus) {
            break;
          }
        }
        finally
        {
          $Response.Close();
        }
      }
      catch {
        $IsSuccessStatus = $false;
        # Eat the exception and try again.
      }
    }

    $Retry++

    # Calculate progress %
    [double]$Progress = 100.0;
    if (!$IsSuccessStatus -and $RetriesEnabled) {
      $Progress = 0.0;

      # Compute progress in range [0.0, 1.0]
      if ($MaxRetries -gt 0) {
        $Progress = [double]$Retry / $MaxRetries;
      }

      if ($Timeout.TotalMilliseconds -gt 0) {
        $Progress = [System.Math]::Max($Progress, [double]$Stopwatch.ElapsedMilliseconds / $Timeout.TotalMilliseconds);
      }

      # Map progress to range [0.0, 100.0]
      $Progress = [System.Math]::Min($Progress, 1.0) * 100.0;
    }

    # Output progress summary.
    if ([string]::Equals($ProgressFormat, 'Progress', [System.StringComparison]::OrdinalIgnoreCase)) {
      Write-Progress -Activity 'Waiting for URL' -Status ("{0:n1}% Complete:" -f $Progress) -PercentComplete $Progress;
    } elseif ([string]::Equals($ProgressFormat, 'VSTS', [System.StringComparison]::OrdinalIgnoreCase)) {
      Write-Host "##vso[task.setprogress value=$Progress;]Waiting for URL";
    }

    # Short-circuit on success.
    if ($IsSuccessStatus) {
      return $true;
    }
  } while ($RetriesEnabled -and (!$TimeoutSet -or ($Stopwatch.ElapsedMilliseconds -lt $Timeout.TotalMilliseconds)) -and (!$MaxRetriesSet -or ($Retry -lt $MaxRetries)) );

  return $false;
}
