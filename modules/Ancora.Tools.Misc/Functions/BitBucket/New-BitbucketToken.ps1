function New-BitbucketToken {
  <#
    .SYNOPSIS
      Provisions a new BitBucket accesss token.

    .PARAMETER Key
      Bitbucket consumer key.

    .PARAMETER Secret
      Bitbucket consumer secret.

    .EXAMPLE
      PS> New-AncBitbucketToken -Key (ConvertTo-SecureString -String 'my-key' -AsPlainText -Force) -Secret (ConvertTo-SecureString -String 'my-secret' -AsPlainText -Force);
  #>
  [OutputType("OAuthToken")]
  [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseShouldProcessForStateChangingFunctions', '', Justification = "Creates in-memory object only.")]
  [CmdletBinding(ConfirmImpact = 'None')]
  param(
    [Parameter(Mandatory=$true)]
    [securestring]$Key,

    [Parameter(Mandatory=$true)]
    [securestring]$Secret
  )

  $ErrorActionPreference = 'Stop';

  $KeyBstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Key);
  $SecretBstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Secret);

  $Resp = Invoke-WebRequest -Uri 'https://bitbucket.org/site/oauth2/access_token' `
      -Method Post `
      -ContentType 'application/x-www-form-urlencoded' `
      -Body @{ grant_type = 'client_credentials' } `
      -Headers @{ Authorization = "Basic " + [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes("$([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($KeyBstr)):$([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($SecretBstr))")) };

  $RespBody = ConvertFrom-Json $([String]::new($Resp.Content));

  $Token = [OAuthToken]::new($RespBody.access_token, $RespBody.refresh_token, $RespBody.scopes);

  return $Token;
}
