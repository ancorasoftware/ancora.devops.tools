# Ancora.DevOps.Tools

[![Ancora.DevOps.Tools package in devops-tools feed in Azure Artifacts](https://feeds.dev.azure.com/ancorasoftware/6289b544-9d54-487f-be43-3ef282df3da4/_apis/public/Packaging/Feeds/f520c733-b510-4d0f-ad97-e56b847ff96a/Packages/c9f338c3-bdf1-4320-a735-927e7a861f55/Badge)](https://dev.azure.com/ancorasoftware/DevOps/_packaging?_a=package&feed=f520c733-b510-4d0f-ad97-e56b847ff96a&package=c9f338c3-bdf1-4320-a735-927e7a861f55&preferRelease=true)

A PowerShell module providing tools supporting Ancora's DevOps processes.

## Installation

### Ancora PowerShell Repository

If you have not already done so, register the Ancora nuget feed as a PowerShell repository:

```powershell
Register-PSRepository -Name 'Ancora DevOps' -SourceLocation https://pkgs.dev.azure.com/ancorasoftware/DevOps/_packaging/devops-tools/nuget/v2 -InstallationPolicy Trusted
```

The PowerShell module can then be installed:


```powershell
Install-Module -Name Ancora.DevOps.Tools
```

## Usage

### Discovering cmdlets

Use the `Get-Command` cmdlet to discover cmdlets within a specific module, or cmdlets that follow a specific search pattern:

```powershell
# List all cmdlets in the Ancora.DevOps.Tools module
Get-Command -Module Ancora.DevOps.Tools

# List all cmdlets that contain VM in the Ancora.DevOps.Tools module
Get-Command -Module Az.Compute -Name '*VM*'
```

### Cmdlet help and examples

To view the help content for a cmdlet, use the Get-Help cmdlet:

```powershell
# View the basic help content for Test-AncUrl
Get-Help -Name Test-AncUrl

# View the examples for Test-AncUrl
Get-Help -Name Test-AncUrl -Examples

# View the full help content for Test-AncUrl
Get-Help -Name Test-AncUrl -Full
```
